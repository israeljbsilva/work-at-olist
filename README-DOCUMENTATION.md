# work-at-olist

### Build
<a href="https://gitlab.com/israeljbsilva/work-at-olist/commits/master"><img alt="build status" src="https://gitlab.com/israeljbsilva/work-at-olist/badges/master/build.svg" /></a>


# Heroku Application Link
- https://work-at-olist-israeljbsilva.herokuapp.com/api-docs/


# Git App Link
- https://gitlab.com/israeljbsilva/work-at-olist

# Tools used:
- Python 3.6
- Django
- Postgres
- Django-Background-Tasks
- Git
- GitLab CI / CD

# Automation tool
This project uses `Makefile` as automation tool.

# Set-up Virtual Environment

The following commands install and set-up `pyenv` tool (https://github.com/pyenv/pyenv) used to create/manage virtual environments:

```bash
$ git clone https://github.com/pyenv/pyenv.git ~/.pyenv
$ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
$ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
$ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init -)"\nfi' >> ~/.bashrc
$ exec "$SHELL"
$ git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
$ echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
$ exec "$SHELL"
```

After that, access the project directory and execute `make all` to recreate the virtual environment and install the dependencies.


# Run the application locally
```
python manage.py runserver
```

# Run the application locally (BACKGROUND TASK)
```
python manage.py process_tasks --log-std
```

# Tests

## Run tests on SQLite
```
$ make test
```

# Code Convention::

## Performs convections and metrics in code:
```
$ make code-convention
```
 
# API Documentation
API documentation is available at the endpoint `/api-docs/`.

# /call-start-record
This endpoint serves to record the start call.

```
{
  "timestamp":  // The timestamp of when the event occured;
  "call_id":  // Unique for each call record pair;
  "source":  // The subscriber phone number that originated the call;
  "destination":  // The phone number receiving the call.
}
```

# /call-end-record
This endpoint serves to record the end call.

```
{
  "timestamp":  // The timestamp of when the event occured;
  "call_id":  // Unique for each call record pair;
}
```

# /phone-bill

Since it is not safe to believe the accuracy of the data received,
consistency, nor expect any order in your orders, a task has been created, which is in the background checking if there is already the complete **call_id** key pair.

If found, the call of that key pair is is calculated and saved in the TelephoneBill model for future viewing through this endpoint.

Each telephone bill call record has the fields:

* destination
* call start date
* call start time
* call duration
* call price

## Queries:
- **subscriber_telephone_number** (required): The subscriber telephone number. Ex: 48984359057
- **reference_period**: The reference period (month/year). Ex: 08/2019
