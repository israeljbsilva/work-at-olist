import uuid
import factory

from django.utils import timezone

from decimal import Decimal

from work_at_olist.models import CallStartRecord, CallEndRecord, TelephoneBill


CALL_ID = 1


class CallStartRecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CallStartRecord

    id = uuid.uuid4()
    timestamp = '2018-02-28T21:57:13Z'
    call_id = CALL_ID
    source = '99988526423'
    destination = '9933468278'


class CallEndRecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CallEndRecord

    id = uuid.uuid4()
    timestamp = '2018-03-01T22:10:56Z'
    call_id = CALL_ID


class TelephoneBillFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TelephoneBill

    call_id = 1
    destination = '48984359051'
    call_start_timestamp = timezone.now()
    call_end_timestamp = timezone.now()
    call_start_time = '06:00:00'
    call_duration = '0:02:46.956000'
    call_price = Decimal('0.54')
    source = '48984359052'
