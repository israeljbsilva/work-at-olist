from work_at_olist import tasks
from work_at_olist.models import TelephoneBill


def test_should_calculate_complete_calls(db, call_start_record, call_end_record):
    # GIVEN
    assert call_start_record
    assert call_end_record

    # WHEN
    tasks.save_calculated_phone_bill()

    # THEN
    telephone_bill = TelephoneBill.objects.filter(call_id=call_start_record.call_id)[0]
    assert telephone_bill.call_id == call_start_record.call_id
    assert telephone_bill.destination == call_start_record.destination
    assert telephone_bill.call_start_timestamp.strftime('%Y-%m-%dT%H:%M:%SZ') == '2018-02-28T21:57:13Z'
    assert telephone_bill.call_start_time == '21:57:13'
    assert telephone_bill.call_duration == '1 day, 0:13:43'
    assert str(telephone_bill.call_price) == '86.94'
