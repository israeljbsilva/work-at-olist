django==2.0.*
django-environ==0.4.*
dj-database-url==0.5.*
drf_yasg==1.11.*
django_extensions==2.1.*
django-choices==1.6.*
drf-nested-routers==0.91
djangorestframework==3.9.*
pillow==2.9.0
psycopg2-binary==2.7.*
whitenoise==3.3.*
django-cors-headers==2.4.*
django-background-tasks==1.1.*

gevent
gunicorn
newrelic